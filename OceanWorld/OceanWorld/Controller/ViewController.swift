//
//  ViewController.swift
//  OceanWorld
//
//  Created by Maxim Vasilenko on 26/12/2018.
//  Copyright © 2018 Maxim Vasilenko. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate,
                      UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    let myWorld = OceanWorld(horizontalSize: Constants.horizontalSize,
                             verticalSize: Constants.verticalSize,
                             startPoint: Constants.startPoint)
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBOutlet weak var collectionView: UICollectionView!

    @IBAction func restart(_ sender: Any) {
        myWorld.restart()
        collectionView.reloadData()
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.visibleSize.width/CGFloat(myWorld.width)
        let height = collectionView.visibleSize.height/CGFloat(myWorld.heigth)
        return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myWorld.length
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "animalCell", for: indexPath)
        guard let itemCell = cell as? AnimalCollectionViewCell else {
            return cell
        }
        switch myWorld.getAnimal(index: indexPath.item)?.typeAnimal {
        case .tux?:
            itemCell.imageView.image = #imageLiteral(resourceName: "tux")
        case .orca?:
            itemCell.imageView.image = #imageLiteral(resourceName: "orca")
        case .none:
            itemCell.imageView.image = nil
        }
        return itemCell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        myWorld.doRound()
        collectionView.reloadData()
    }

}
