//
//  Constants.swift
//  OceanWorld
//
//  Created by Maxim Vasilenko on 26/12/2018.
//  Copyright © 2018 Maxim Vasilenko. All rights reserved.
//

import Foundation

struct Constants {
    static let horizontalSize: Int = 10
    static let verticalSize: Int = 15
    static let startPoint: Point = Point(xCoordinate: 0, yCoordinate: 0, zCoordinate: 0)
    static let orcaPercent: Double = 5.0
    static let tuxPercent: Double = 50.0
    static let stepsToReproduceTux: Int = 3
    static let stepsToReproduceOrca: Int = 8
    static let orcaDeathHunger: Int = 3
}
