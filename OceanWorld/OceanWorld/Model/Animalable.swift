//
//  AnimalablePrototype.swift
//  OceanWorld
//
//  Created by Maxim Vasilenko on 26/12/2018.
//  Copyright © 2018 Maxim Vasilenko. All rights reserved.
//

import Foundation

enum AnimalsType {
    case orca
    case tux
}

protocol Animalable {
    var stepsToReproduce: Int { get }
    var myCountSteps: Int { get set }
    var isDidStep: Bool { get set }
    var myIndex: Int { get set }
    var typeAnimal: AnimalsType { get }

    func tryAction(myWorld: OceanWorld)
}

extension Animalable {

    func tryMove( myWorld: OceanWorld) -> Int? {
        guard let neighbourEmptySite = myWorld.getRandomNeighbourEmptySite(index: myIndex) else { return nil }
        myWorld.setAnimal(animal: self, index: neighbourEmptySite)
        myWorld.setAnimal(animal: nil, index: myIndex)
        return neighbourEmptySite
    }

    func tryReproduce(myWorld: OceanWorld) {
        guard let neighbourEmptySite = myWorld.getRandomNeighbourEmptySite(index: myIndex) else { return }
        var newAnimal: Animalable
        switch self.typeAnimal {
        case .tux:
            newAnimal = Tux()
        case .orca:
            newAnimal = Orca()
        }
        newAnimal.myIndex = neighbourEmptySite
        myWorld.setAnimal(animal: newAnimal, index: neighbourEmptySite)
    }

}
