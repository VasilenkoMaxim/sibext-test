//
//  DiscreteWorld.swift
//  OceanWorld
//
//  Created by Maxim Vasilenko on 27/12/2018.
//  Copyright © 2018 Maxim Vasilenko. All rights reserved.
//

import Foundation

struct Point {
    var xCoordinate: Int
    var yCoordinate: Int
    var zCoordinate: Int

    static func + ( first: Point, second: Point) -> Point {
        return Point( xCoordinate: first.xCoordinate + second.xCoordinate,
                      yCoordinate: first.yCoordinate + second.yCoordinate,
                      zCoordinate: first.zCoordinate + second.zCoordinate)
    }

    static func - ( first: Point, second: Point) -> Point {
        return Point( xCoordinate: first.xCoordinate - second.xCoordinate,
                      yCoordinate: first.yCoordinate - second.yCoordinate,
                      zCoordinate: first.zCoordinate - second.zCoordinate)
    }

    static func == ( first: Point, second: Point) -> Bool {
        return (first.xCoordinate == second.xCoordinate) &&
               (first.yCoordinate == second.yCoordinate) &&
               (first.zCoordinate == second.zCoordinate)
    }

}

protocol DiscreteWorld {
    var width: Int { get }
    var heigth: Int { get }
    var depth: Int { get }
    var length: Int { get }
    var begin: Point { get }

    func getCoordinate( index: Int ) -> Point
    func getIndex( point: Point ) -> Int
    func getFirstNeighbours( index: Int) -> [Int]
}
