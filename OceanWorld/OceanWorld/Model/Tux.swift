//
//  TuxClass.swift
//  OceanWorld
//
//  Created by Maxim Vasilenko on 26/12/2018.
//  Copyright © 2018 Maxim Vasilenko. All rights reserved.
//

import Foundation

class Tux: Animalable {
    let stepsToReproduce = Constants.stepsToReproduceTux
    var myCountSteps = 0
    var isDidStep = true
    var myIndex = -1
    var typeAnimal = AnimalsType.tux

    func tryAction(myWorld: OceanWorld) {
        guard !isDidStep else { return }
        myCountSteps += 1
        isDidStep = true
        if myCountSteps % stepsToReproduce == 0 {
            tryReproduce(myWorld: myWorld)
        } else {
            if let newIndex = tryMove(myWorld: myWorld) {
                myIndex = newIndex
            }
        }
    }

}
