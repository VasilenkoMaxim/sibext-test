//
//  DiscreteWorld2D.swift
//  OceanWorld
//
//  Created by Maxim Vasilenko on 27/12/2018.
//  Copyright © 2018 Maxim Vasilenko. All rights reserved.
//

import Foundation

private let firstNeighbours = [Point(xCoordinate: -1, yCoordinate: -1, zCoordinate: 0),
                               Point(xCoordinate: -1, yCoordinate: 0, zCoordinate: 0),
                               Point(xCoordinate: -1, yCoordinate: 1, zCoordinate: 0),
                               Point(xCoordinate: 0, yCoordinate: -1, zCoordinate: 0),
                               Point(xCoordinate: 0, yCoordinate: 1, zCoordinate: 0),
                               Point(xCoordinate: 1, yCoordinate: -1, zCoordinate: 0),
                               Point(xCoordinate: 1, yCoordinate: 0, zCoordinate: 0),
                               Point(xCoordinate: 1, yCoordinate: 1, zCoordinate: 0)]

class DiscreteWorld2D: DiscreteWorld {
    var width: Int
    var heigth: Int
    var depth: Int = 1
    var length: Int
    var begin: Point

    init(horizontalSize: Int, verticalSize: Int, startPoint: Point) {
        guard horizontalSize>2 && verticalSize>2 else {
            fatalError("horizontalSize must be >2 and verticalSize must be >2")
        }
        width = horizontalSize
        heigth = verticalSize
        length = width * heigth
        begin = startPoint
    }

    func getCoordinate(index: Int) -> Point {
        let indexNew = conversIndex(index: index)
        let point = Point(xCoordinate: indexNew % width, yCoordinate: indexNew / width, zCoordinate: 0)
        return point + begin
    }

    func getIndex( point: Point) -> Int {
        let newPoint = conversCoordinate( point: point)
        let difference = newPoint - begin
        return length*difference.zCoordinate + width*difference.yCoordinate + difference.xCoordinate
    }

    func getFirstNeighbours(index: Int) -> [Int] {
        let coordinate = getCoordinate(index: index)
        var indexes = [Int]()
        firstNeighbours.forEach { firstNeighbour in
            let newIndex = getIndex( point: coordinate + firstNeighbour)
            indexes.append(newIndex)
        }
        return indexes
    }

    // MARK: Private

    private func conversIndex(index: Int) -> Int {
        if index >= 0 {
            return index % length
        } else {
            return (length + index % length) % length
        }
    }

    private func conversCoordinate(point: Point) -> Point {
        let difference = point - begin
        let xCoordinate = conversCoordinate(coordinate: difference.xCoordinate, size: width)
        let yCoordinate = conversCoordinate(coordinate: difference.yCoordinate, size: heigth)
        let zCoordinate = conversCoordinate(coordinate: difference.zCoordinate, size: depth)
        return begin + Point(xCoordinate: xCoordinate, yCoordinate: yCoordinate, zCoordinate: zCoordinate)
    }

    private func conversCoordinate(coordinate: Int, size: Int) -> Int {
        if coordinate >= size {
            return coordinate % size
        } else {
            if coordinate < 0 {
                return size - 1 + (1 + coordinate) % size
            } else {
                return coordinate
            }
        }
    }

}
