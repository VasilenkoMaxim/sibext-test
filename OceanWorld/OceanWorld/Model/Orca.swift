//
//  OrcaClass.swift
//  OceanWorld
//
//  Created by Maxim Vasilenko on 26/12/2018.
//  Copyright © 2018 Maxim Vasilenko. All rights reserved.
//

import Foundation

class Orca: Animalable {
    private var hunger = 0
    let stepsToReproduce = Constants.stepsToReproduceOrca
    var myCountSteps = 0
    var isDidStep = true
    var myIndex = -1
    var typeAnimal = AnimalsType.orca

    func tryAction(myWorld: OceanWorld) {
        guard !isDidStep else { return }
        myCountSteps += 1
        hunger += 1
        isDidStep = true
        if myCountSteps % stepsToReproduce == 0 {
            tryReproduce(myWorld: myWorld)
        } else if !tryEat(myWorld: myWorld) {
            if let newIndex = tryMove(myWorld: myWorld) {
                myIndex = newIndex
            }
        }
        tryDie(myWorld: myWorld)
    }

    // MARK: Private

    private func tryDie(myWorld: OceanWorld) {
        guard hunger == Constants.orcaDeathHunger else { return }
        myWorld.setAnimal(animal: nil, index: myIndex)
    }

    private func tryEat(myWorld: OceanWorld) -> Bool {
        guard let neighbourTux = myWorld.getRandomNeighbourTux(index: myIndex) else { return false }
        myWorld.setAnimal(animal: self, index: neighbourTux)
        myWorld.setAnimal(animal: nil, index: myIndex)
        myIndex = neighbourTux
        hunger = 0
        return true
    }

}
