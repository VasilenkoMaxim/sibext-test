//
//  WorldClass.swift
//  OceanWorld
//
//  Created by Maxim Vasilenko on 26/12/2018.
//  Copyright © 2018 Maxim Vasilenko. All rights reserved.
//

import Foundation

class OceanWorld: DiscreteWorld2D {
    private let orcaPercent = Constants.orcaPercent
    private let tuxPercent = Constants.tuxPercent
    private var animalsArray = [Animalable?]()

    override init(horizontalSize: Int, verticalSize: Int, startPoint: Point) {
        super.init(horizontalSize: horizontalSize, verticalSize: verticalSize, startPoint: startPoint)
        animalsArray = Array(repeating: Animalable?.none, count: length)
        initAnimals()
    }

    func doRound() {
        readyToActionAnimals()
        for index in 0..<animalsArray.count {
            animalsArray[index]?.tryAction(myWorld: self)
        }
    }

    func restart() {
        initAnimals()
    }

    func getAnimal(index: Int) -> Animalable? {
        return animalsArray[index]
    }

    func setAnimal(animal: Animalable?, index: Int) {
        animalsArray[index] = animal
    }

    func getRandomNeighbourTux(index: Int) -> Int? {
        return getNeighboursTux(index: index).randomElement()
    }

    func getRandomNeighbourEmptySite(index: Int) -> Int? {
        return getNeighboursEmptySite(index: index).randomElement()
    }

    // MARK: Private

    private func initAnimals() {
        guard Constants.orcaPercent + Constants.tuxPercent <= 100.0 else {
            fatalError("Sum of Constants.orcaPercent and Constants.tuxPercent must be <=100")
        }
        let orcaCount = Int( (Double(length) * Constants.orcaPercent / 100.0).rounded() )
        let tuxCount = Int( (Double(length) * Constants.tuxPercent / 100.0).rounded() )
        let animalCount = orcaCount + tuxCount
        for index in 0..<orcaCount {
            animalsArray[index] = Orca()
        }
        for index in orcaCount..<animalCount {
            animalsArray[index] = Tux()
        }
        for index in animalCount..<animalsArray.count {
            animalsArray[index] = nil
        }
        animalsArray.shuffle()
        setAnimalIndexes()
    }

    private func setAnimalIndexes() {
        for index in 0..<animalsArray.count {
            animalsArray[index]?.myIndex = index
        }
    }

    private func readyToActionAnimals() {
        for index in 0..<animalsArray.count {
            animalsArray[index]?.isDidStep = false
        }
    }

    private func getNeighboursTux(index: Int) -> [Int] {
        var neighboursTux = [Int]()
        for neighbour in getFirstNeighbours(index: index) where getAnimal(index: neighbour) is Tux {
            neighboursTux.append(neighbour)
        }
        return neighboursTux
    }

    private func getNeighboursEmptySite(index: Int) -> [Int] {
        var neighboursEmptySite = [Int]()
        for neighbour in getFirstNeighbours(index: index) where getAnimal(index: neighbour) == nil {
            neighboursEmptySite.append(neighbour)
        }
        return neighboursEmptySite
    }
}
