//
//  Unit_DiscreteWorld2DTest.swift
//  OceanWorldTests
//
//  Created by Maxim Vasilenko on 14/01/2019.
//  Copyright © 2019 Maxim Vasilenko. All rights reserved.
//

import XCTest
@testable import OceanWorld

class DiscreteWorld2DMock: DiscreteWorld2D {

}

class UnitDiscreteWorld2DTest: XCTestCase {

    func testGetCoordinate() {
        var discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 3, verticalSize: 5,
                                                  startPoint: Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getCoordinate(index: 0)! == Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getCoordinate(index: 10)! == Point(xCoordinate: 0, yCoordinate: 6, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getCoordinate(index: 15)! == Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getCoordinate(index: 20)! == Point(xCoordinate: 1, yCoordinate: 4, zCoordinate: 10))

        discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 0, verticalSize: 5,
                                              startPoint: Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getCoordinate(index: -1) == nil )

        discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 0, verticalSize: 0,
                                              startPoint: Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getCoordinate(index: 74)! == Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))

        discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 2, verticalSize: 5,
                                              startPoint: Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getCoordinate(index: 4)! == Point(xCoordinate: -1, yCoordinate: 5, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getCoordinate(index: 12)! == Point(xCoordinate: -1, yCoordinate: 4, zCoordinate: 10))

        discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 1, verticalSize: 5,
                                              startPoint: Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getCoordinate(index: 10)! == Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getCoordinate(index: 7)! == Point(xCoordinate: -1, yCoordinate: 5, zCoordinate: 10) )

        discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 3, verticalSize: 2,
                                              startPoint: Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getCoordinate(index: 10)! == Point(xCoordinate: 0, yCoordinate: 4, zCoordinate: 10) )
        XCTAssert( discreteWorld2D.getCoordinate(index: 2)! == Point(xCoordinate: 1, yCoordinate: 3, zCoordinate: 10) )

        discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 3, verticalSize: 1,
                                              startPoint: Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getCoordinate(index: 1)! == Point(xCoordinate: 0, yCoordinate: 3, zCoordinate: 10) )
        XCTAssert( discreteWorld2D.getCoordinate(index: 8)! == Point(xCoordinate: 1, yCoordinate: 3, zCoordinate: 10) )
    }

    func testGetIndex() {
        var discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 3, verticalSize: 5,
                                                  startPoint: Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10)) == 0 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: 0, yCoordinate: 5, zCoordinate: 10)) == 7 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: 2, yCoordinate: 6, zCoordinate: 10)) == 9 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: -3, yCoordinate: 4, zCoordinate: 10)) == 4 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: -1, yCoordinate: 12, zCoordinate: 10)) == 12 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: 1, yCoordinate: 0, zCoordinate: 10)) == 8 )

        discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 0, verticalSize: 0,
                                              startPoint: Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: 2, yCoordinate: 6, zCoordinate: 10)) == 0 )

        discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 2, verticalSize: 5,
                                              startPoint: Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: -1, yCoordinate: 5, zCoordinate: 10)) == 4 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: -3, yCoordinate: 7, zCoordinate: 1)) == 8 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: 0, yCoordinate: -1, zCoordinate: 13)) == 3 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: 1, yCoordinate: 3, zCoordinate: 4)) == 0 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: 0, yCoordinate: 12, zCoordinate: 7)) == 9 )

        discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 1, verticalSize: 5,
                                              startPoint: Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: -1, yCoordinate: 5, zCoordinate: 10)) == 2 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: -2, yCoordinate: 7, zCoordinate: 1)) == 4 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: -1, yCoordinate: -1, zCoordinate: 13)) == 1 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: 0, yCoordinate: 3, zCoordinate: 4)) == 0 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: -1, yCoordinate: 12, zCoordinate: 7)) == 4 )

        discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 3, verticalSize: 2,
                                              startPoint: Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: -4, yCoordinate: 3, zCoordinate: 10)) == 0 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: 0, yCoordinate: 2, zCoordinate: 1)) == 4 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: 4, yCoordinate: 4, zCoordinate: 13)) == 5 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: 0, yCoordinate: 5, zCoordinate: 4)) == 1 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: 1, yCoordinate: 3, zCoordinate: 7)) == 2 )

        discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 3, verticalSize: 1,
                                              startPoint: Point(xCoordinate: -1, yCoordinate: 3, zCoordinate: 10))
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: -4, yCoordinate: 3, zCoordinate: 10)) == 0 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: 0, yCoordinate: 2, zCoordinate: 1)) == 1 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: 4, yCoordinate: 3, zCoordinate: 13)) == 2 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: 0, yCoordinate: 4, zCoordinate: 4)) == 1 )
        XCTAssert( discreteWorld2D.getIndex(point: Point(xCoordinate: 1, yCoordinate: 3, zCoordinate: 7)) == 2 )
    }

    func testGetFirstNeighbours() {
        var discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 0, verticalSize: 1,
                                                  startPoint: Point(xCoordinate: 0, yCoordinate: 0, zCoordinate: 0))
        XCTAssert( discreteWorld2D.getFirstNeighbours(index: 0) == [] )

        discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 1, verticalSize: 0,
                                              startPoint: Point(xCoordinate: 0, yCoordinate: 0, zCoordinate: 0))
        XCTAssert( discreteWorld2D.getFirstNeighbours(index: 0) == [] )

        discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 1, verticalSize: 1,
                                              startPoint: Point(xCoordinate: 0, yCoordinate: 0, zCoordinate: 0))
        XCTAssert( discreteWorld2D.getFirstNeighbours(index: 0).isEmpty )

        discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 2, verticalSize: 2,
                                              startPoint: Point(xCoordinate: 0, yCoordinate: 0, zCoordinate: 0))
        XCTAssert( discreteWorld2D.getFirstNeighbours(index: 0).count == 3 )
        var check: Bool = true
        [ 1, 2, 3].forEach { item in check = discreteWorld2D.getFirstNeighbours(index: 0).contains(item) && check }
        XCTAssert( check )

        XCTAssert( discreteWorld2D.getFirstNeighbours(index: 3).count == 3 )
        check = true
        [ 0, 1, 2].forEach { item in check = discreteWorld2D.getFirstNeighbours(index: 3).contains(item) && check }
        XCTAssert( check )

        discreteWorld2D = DiscreteWorld2DMock(horizontalSize: 3, verticalSize: 3,
                                              startPoint: Point(xCoordinate: 0, yCoordinate: 0, zCoordinate: 0))

        XCTAssert( discreteWorld2D.getFirstNeighbours(index: 0).count == 8 )
        check = true
        [ 1, 2, 3, 4, 5, 6, 7, 8].forEach { item in
            check = discreteWorld2D.getFirstNeighbours(index: 0).contains(item) && check
        }
        XCTAssert( check )

        XCTAssert( discreteWorld2D.getFirstNeighbours(index: 3).count == 8 )
        check = true
        [ 0, 1, 2, 4, 5, 6, 7, 8].forEach { item in
            check = discreteWorld2D.getFirstNeighbours(index: 3).contains(item) && check
        }
        XCTAssert( check )

        XCTAssert( discreteWorld2D.getFirstNeighbours(index: 8).count == 8 )
        check = true
        [ 0, 1, 2, 3, 4, 5, 6, 7].forEach { item in
            check = discreteWorld2D.getFirstNeighbours(index: 8).contains(item) && check
        }
        XCTAssert( check )
    }

}
